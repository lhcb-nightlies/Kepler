// Local
#include "TbRawFile.h"

//=============================================================================
// Standard constructor
//=============================================================================
TbRawFile::TbRawFile(const std::string& filename,
                     TbHeaderDecoder* headerDecoder)
    : TbBufferedFile(filename, 0) {

  if (!is_open()) {
    m_good = false;
    return;
  }

  // Determine the index of the file in the sequence.
  const size_t pos = m_filename.find(".dat");
  const size_t dash = m_filename.find_last_of("-");
  if (pos == std::string::npos) {
    m_good = false;
    return;
  }
  if (!(pos - dash > 1)) {
    m_good = false;
    return;
  }
  m_split_index = std::stoi(m_filename.substr(dash + 1, pos - dash - 1));
  if (m_split_index == 1) initialise();

  // Read the magic pattern at the beginning of the file.
  uint32_t magic = 0;
  if (m_file->ReadBuffer((char*)&magic, 0, 4)) {
    m_good = false;
    return;
  }
  // Make sure this is a SPIDR file.
  if (magic != 0x52445053) {
    std::cerr << "Unexpected magic pattern (" << magic << ")." << std::endl;
    m_good = false;
    return;
  }
  // Read the total header size.
  if (m_file->ReadBuffer((char*)&m_headerSize, 4, 4)) {
    m_good = false;
    return;
  }
  if (m_headerSize > 66304) m_headerSize = 66304;
  // Read and decode the header.
  char* devHeader = new char[m_headerSize];
  if (m_file->ReadBuffer(devHeader, 0, m_headerSize)) {
    m_good = false;
    return;
  }
  if (!headerDecoder->read(devHeader, m_deviceType, m_deviceId)) {
    m_good = false;
    return;
  }
  delete[] devHeader;
  if (m_deviceType == 1) {
    // Get the VeloPix ID from the file name.
    size_t p = m_filename.find("vpxdaq");
    if (p == std::string::npos) {
      // This is probably a run with the timepix3 telescope
      p = m_filename.find("tpx3daq");
      if (p == std::string::npos) {
        std::cout << "Failed to parse velopix data filename: " << m_filename << std::endl;
        exit(1);
      } else {
        m_deviceId = m_filename.substr(p - 8, 7);
      }
    } else {
      // This is a run with the velopix telescope
      const int daq = std::stoi(m_filename.substr(p + 6, 2)) - 1;
      const int chip = daq / 2;
      const int dev = (daq % 2) * 2 + std::stoi(m_filename.substr(p + 12, 1));
      m_deviceId = "Vpx" + std::to_string(chip) + "_" + std::to_string(dev);
    }
  }
  m_packetSize = m_deviceType == 0 ? 8 : 6;
  // Move to the first data packet.
  m_file->Seek(m_headerSize);
  const uint64_t fileSize = m_file->GetSize();
  m_nPackets = (fileSize - m_headerSize) / m_packetSize;
  m_bytesRead = m_headerSize;
}
