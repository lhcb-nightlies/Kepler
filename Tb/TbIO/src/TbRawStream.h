#pragma once

#include "GaudiAlg/GaudiTool.h"
#include "Event/TbHit.h"
#include "Event/TbTrigger.h"
#include "TbKernel/TbFunctors.h"
#include "TbKernel/TbConstants.h"
#include "TbRawFile.h"

static const InterfaceID IID_TbRawStream("TbRawStream", 1, 0);

class TbRawStream : public GaudiTool {

 public:
  /// Constructor
  TbRawStream(const std::string& type, const std::string& name,
              const IInterface* parent);

  static const InterfaceID& interfaceID() { return IID_TbRawStream; }

  uint64_t timer() const { return m_timer; }
  unsigned int lsb() const { return m_lsb; }
  /// Set the clock explicitly, if device sync fails
  void setTimer(const uint64_t timer) { m_timer = timer; }
  void setLSB(const unsigned int lsb) { m_lsb = lsb; }

  template <typename T>
  std::vector<T>* cache();
  std::vector<LHCb::TbHit*>& hitCache() { return m_hitCache; }
  std::vector<LHCb::TbTrigger*>& trgCache() { return m_trgCache; }

  void insert(LHCb::TbTrigger* packet);
  void insert(LHCb::TbHit* packet);

  void clearCache();

  std::vector<TbRawFile*>& files() { return m_files; }
  void addFile(TbRawFile* file) { m_files.push_back(file); }
  void close() {
    for (auto& raw_file : m_files) {
      if (raw_file->is_open()) raw_file->close();
    }
  }

  /// Return the next data packet in the stream.
  uint64_t getNext() {
    return m_deviceType == 1 ? getNextVpx() : (*m_currentFile)->getNext();
  }

  /// Return the file size (in number of packets).
  unsigned int nPackets() const { return m_nPackets; }
  /// Return whether the end of the stream has been reached
  bool eos();

  void prepare();

  /// Move to a particular time in the stream
  void fastForward(const uint64_t timeToSkipTo);

  int addTimingPacket(const uint64_t data_packet);

  unsigned int device() const { return m_device; }
  unsigned int plane() const { return m_plane; }
  unsigned int col() const { return m_col; }
  unsigned int deviceType() const { return m_deviceType; }

  void setDevice(const unsigned int device) { m_device = device; }
  void setPlane(const unsigned int plane) { m_plane = plane; }
  void setCol(const unsigned int col) { m_col = col; }
  void setDeviceType(const unsigned int devtyp) { m_deviceType = devtyp; }

  uint64_t m_tpx3Timer = 0;
  unsigned int m_orbit = 0;
  bool m_clockLatch=0;
  uint64_t m_vpxTimer = 0;
 private:
  static const int64_t m_tenSeconds;
  static const int64_t m_maxTimeDifference;

  std::vector<TbRawFile*>::iterator m_currentFile;
  std::vector<TbRawFile*> m_files;
  std::vector<LHCb::TbHit*> m_hitCache;
  std::vector<LHCb::TbTrigger*> m_trgCache;

  unsigned int m_device;
  unsigned int m_plane;
  unsigned int m_col = 0;
  unsigned int m_deviceType = 0;

  /// Total number of packets in the stream.
  uint64_t m_nPackets = 0;
  /// Temporary lsb of the global timer
  unsigned int m_lsb = 0;
  uint64_t m_timer = 0;

  /// Buffer of VeloPix packets.
  std::array<uint64_t, 4> m_packets;
  /// Current position in the buffer of VeloPix packets.
  unsigned int m_packetIndex = 3;
  /// Return the next pixel packet in a VeloPix data file.
  uint64_t getNextVpx();
  /// Return the previous pixel packet in a VeloPix data file.
  uint64_t getPreviousVpx();
 
  /// Binary search for a given time in the stream.
  void coarseFastForward(const double timeToSkipTo);
  /// Linear search for a given time in the stream.
  void fineFastForward(const uint64_t timeToSkipTo);
  uint64_t getCurrentTime();

  bool setMSB(uint64_t msb);
};
