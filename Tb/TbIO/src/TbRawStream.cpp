#include <cmath>
#include "TbRawStream.h"

DECLARE_TOOL_FACTORY(TbRawStream)

const int64_t TbRawStream::m_tenSeconds = 256 * std::pow(10, 11);
const int64_t TbRawStream::m_maxTimeDifference = std::pow(2, 40);

//=============================================================================
// Standard constructor
//=============================================================================
TbRawStream::TbRawStream(const std::string& type, const std::string& name,
                         const IInterface* parent)
    : GaudiTool(type, name, parent) {

  declareInterface<TbRawStream>(this);
}


//=============================================================================
// Return whether the end of the stream has been reached
//=============================================================================
bool TbRawStream::eos() {

  if (unlikely(m_currentFile == m_files.end())) return true;
  if (unlikely((*m_currentFile)->eof() == true)) {
    // Move to the next file in the sequence.
    (*m_currentFile)->close();
    m_currentFile++;
    if (m_currentFile != m_files.end()) (*m_currentFile)->initialise();
  }
  return unlikely(m_currentFile == m_files.end());
}

//=============================================================================
// Return the next pixel packet in a VeloPix data file
//=============================================================================
uint64_t TbRawStream::getNextVpx() {

  // Each VeloPix packet has six bytes. For reasons of efficiency, four
  // VeloPix packets are packed in three eight-byte words.
  ++m_packetIndex;
  if (m_packetIndex == 4) {
    m_packets = {0, 0, 0, 0};
    // Read a new group of three eight-byte words.
    const uint64_t word0 = !eos() ? (*m_currentFile)->getNext() : 0;
    const uint64_t word1 = !eos() ? (*m_currentFile)->getNext() : 0;
    const uint64_t word2 = !eos() ? (*m_currentFile)->getNext() : 0;
    m_packets[0] = word0 & 0x0000FFFFFFFFFFFFull;
    m_packets[1] = (((uint64_t)(word0 >> 48)) & 0x000000000000FFFFull) |
                   (((uint64_t)(word1 << 16)) & 0x0000FFFFFFFF0000ull);
    m_packets[2] = (((uint64_t)(word1 >> 32)) & 0x00000000FFFFFFFFull) |
                   (((uint64_t)(word2 << 32)) & 0x0000FFFF00000000ull);
    m_packets[3] =  ((uint64_t)(word2 >> 16)) & 0x0000FFFFFFFFFFFFull;
    m_packetIndex = 0;
  }
  return m_packets[m_packetIndex];
}

//=============================================================================
// Return the previous pixel packet in a VeloPix data file
//=============================================================================
uint64_t TbRawStream::getPreviousVpx() {

  if (m_packetIndex == 0) {
    // Read the previous new group of three eight-byte words.
    const uint64_t word2 = (*m_currentFile)->getPrevious();
    const uint64_t word1 = (*m_currentFile)->getPrevious();
    const uint64_t word0 = (*m_currentFile)->getPrevious();
    m_packets[0] = word0 & 0x0000FFFFFFFFFFFFull;
    m_packets[1] = (((uint64_t)(word0 >> 48)) & 0x000000000000FFFFull) |
                   (((uint64_t)(word1 << 16)) & 0x0000FFFFFFFF0000ull);
    m_packets[2] = (((uint64_t)(word1 >> 32)) & 0x00000000FFFFFFFFull) |
                   (((uint64_t)(word2 << 32)) & 0x0000FFFF00000000ull);
    m_packets[3] =  ((uint64_t)(word2 >> 16)) & 0x0000FFFFFFFFFFFFull;
    m_packetIndex = 4;
  }
  --m_packetIndex;
  return m_packets[m_packetIndex];
}

//=============================================================================
// Move to a particular time in the stream
//=============================================================================
void TbRawStream::fastForward(const uint64_t timeToSkipTo) {
  // Go one second before we need to
  coarseFastForward(timeToSkipTo / Tb::second);
  fineFastForward(timeToSkipTo);
}

//=============================================================================
// Binary search of the stream to find a particular time
//=============================================================================
void TbRawStream::coarseFastForward(const double timeToSkipTo) {

  info() << "Skipping forward to time = " << timeToSkipTo << " s" << endmsg;
  // Need to work out which file to read first.
  if (m_files.size() > 1) {
    for (auto it = m_files.begin(), end = m_files.end(); it != end; ++it) {
      m_currentFile = it;
      (*it)->initialise();
      const double timeInSeconds = getCurrentTime() * Tb::ToA / Tb::second;
      if (timeInSeconds > timeToSkipTo) {
        (*it)->reset();
        m_currentFile--;
        break;
      }
      (*it)->reset();
    }
  }

  (*m_currentFile)->initialise();
  uint64_t dt = (*m_currentFile)->nPackets() / 2;
  uint64_t pos = dt;
  uint64_t timer = 0;
  double timeInSeconds = 0.;
  while (!eos() && fabs(timeToSkipTo - timeInSeconds) > 0.6 && dt > 1) {
    // Scroll to the new position
    (*m_currentFile)->setOffset(pos);
    dt /= 2;
    timer = getCurrentTime();
    timeInSeconds = timer * Tb::ToA / Tb::second;
    pos = timeToSkipTo > timeInSeconds ? (pos + dt) : (pos - dt);
  }
  if (dt <= 1) Warning("Binary search has failed!").ignore();
  m_timer = timer << 12;
}

//=============================================================================
// Scroll through the stream to find a particular time
//=============================================================================
void TbRawStream::fineFastForward(const uint64_t timeToSkipTo) {
  uint64_t currentTime(0);
  while (!eos() && currentTime < timeToSkipTo) {
    const uint64_t packet = getNext();
    if (m_deviceType == 0) {
      const unsigned int header = packet >> 60;
      if (header == 0xA || header == 0xB) {
        uint64_t global_time = timer();
        uint64_t packet_time = (0xFFFF & packet) << 26;
        const int diff =
            (0x3 & (global_time >> 40)) - (0x3 & (packet_time >> 40));
        constexpr uint64_t one = (uint64_t)(1) << 40;
        if (diff == 1 || diff == -3)
          global_time = global_time - one;
        else if (diff == -1 || diff == 3)
          global_time = global_time + one;
      } else if (header == 0x4) {
        addTimingPacket(packet);
        currentTime = m_timer;
      }
    } else {
      // TODO
      Error("fineFastFoward: not implemented for Vpx");
      return;
    }
  }
}

//=============================================================================
// Get the timestamp of the next packet in the stream
//=============================================================================
uint64_t TbRawStream::getCurrentTime() {
  m_lsb = 0;
  uint64_t currentTime = 0;
  bool gotTime = false;
  while (!eos() && !gotTime) {
    const uint64_t packet = getNext();
    if (m_deviceType == 0) {
      const unsigned int header = packet >> 60;
      if (header == 0x4 && (0xF & (packet >> 54)) != 0xF) {
        const unsigned int subheader = 0xF & (packet >> 56);
        if (subheader == 0x4) {
          m_lsb = 0xFFFFFFFF & (packet >> 16);
        } else if (subheader == 0x5 && m_lsb != 0) {
          const uint64_t msb = 0xFFFFFFFF & (packet >> 16);
          currentTime = (m_lsb + (msb << 32));
          gotTime = true;
        }
      }
    } else {
      // TODO
      Error("getCurrentTime: not implemented for Vpx");
      return 0;
    }
  }
  // TODO: << 12, such that we return global time?
  return currentTime;
}

//=============================================================================
//
//=============================================================================
int TbRawStream::addTimingPacket(const uint64_t packet) {
  const unsigned int subheader = 0xF & (packet >> 56);
  int state = 1;
  if (subheader == 0x5) {
    // info() << "Current msb = Setting msb of clock: 0x" <<
    // std::hex << packet <<  std::dec <<endmsg;
    if (setMSB(0xFFFFFFFF & (packet >> 16)) == 0) state = 2;
  } else if (subheader == 0x4) {
    // info() << "Setting lsb of stream: 0x" << std::hex <<
    // packet << ", current = " << m_lsb << std::dec << endmsg;
    setLSB(0xFFFFFFFF & (packet >> 16));
  } else {
    state = 0;
  }
  return state;
}

//=============================================================================
// Update the MSB of the timer.
//=============================================================================
bool TbRawStream::setMSB(uint64_t msb) {
  uint64_t currentTime = (m_lsb + (msb << 32)) << 12;
  const int64_t timeDifference = currentTime - m_timer;
  if (timeDifference > m_tenSeconds) {
    // Detected jump of greater than 10s
    // Info("Jump detected. This is ok, probably means " +
    //      "a bit has been updated between LSB and MSB").ignore();
    if (msb > 0) msb -= 1;
    currentTime = (m_lsb + (msb << 32)) << 12;
  }
  if (timeDifference > m_maxTimeDifference ||
      timeDifference < -m_maxTimeDifference) {
    // warning << "Current global time = " << m_timer / Tb::second
    //         << " s. Clock is trying to update to "
    //         << currentTime / Tb::second << " s" << endmsg;
    return false;
  }
  m_timer = currentTime;
  // info() <<  "Current global time: "
  //        << currentTime / Tb::second << " s" <<endmsg;
  return true;
}

//=============================================================================
// Open the first file in the sequence and move to the first pixel packet
//=============================================================================
void TbRawStream::prepare() {
  m_currentFile = m_files.begin();
  (*m_currentFile)->initialise();
  m_nPackets = 0;
  for (const auto& f : m_files) m_nPackets += f->nPackets();
  // Find the first pixel hit
  unsigned int prep_packets(0);
  while (!eos()) {
    const uint64_t packet = getNext();
    ++prep_packets;
    if (m_deviceType == 0) {
      const uint64_t header = 0xF & (packet >> 60);
      if (header == 0xA || header == 0xB) break;
    } else if (m_deviceType == 1) {
      // The most significant bit of the six-byte word indicates
      // whether this is a true packet or a dummy packet.
      // info() << format("0x%llx", packet) << endmsg;
      if ((0x1 & (packet >> 47)) == 1) break;
    } else {
      Error("Unknown device type (" + std::to_string(m_deviceType) + ")");
    }
  }
  if (eos()) Error("Reached eos while searching for pixel packets.");
  // info() << "Number of prep packets skipped = " << prep_packets << endmsg;
  m_nPackets = m_nPackets - prep_packets + 1;
  if (m_deviceType == 0) {
    (*m_currentFile)->getPrevious();
  } else {
    getPreviousVpx();
  }

}

//=============================================================================
// Return hit/trigger cache (templated function)
//=============================================================================
template <>
std::vector<LHCb::TbHit*>* TbRawStream::cache() {
  return &m_hitCache;
}
template <>
std::vector<LHCb::TbTrigger*>* TbRawStream::cache() {
  return &m_trgCache;
}

//=============================================================================
// Add a hit/trigger to the cache
//=============================================================================
void TbRawStream::insert(LHCb::TbHit* hit) { m_hitCache.push_back(hit); }
void TbRawStream::insert(LHCb::TbTrigger* trg) { m_trgCache.push_back(trg); }

//=============================================================================
// Clear the hit/trigger caches
//=============================================================================
void TbRawStream::clearCache() {

  for (auto it = m_hitCache.begin(); it != m_hitCache.end(); ++it) {
    if (*it) delete *it;
  }
  m_hitCache.clear();
  for (auto it = m_trgCache.begin(); it != m_trgCache.end(); ++it) {
    if (*it) delete *it;
  }
  m_trgCache.clear();
}

