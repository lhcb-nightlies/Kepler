#include <algorithm>

// Gaudi
#include "GaudiKernel/IEventProcessor.h"
#include "GaudiAlg/ISequencerTimerTool.h"

// Tb/TbKernel
#include "TbKernel/TbFunctors.h"
#include "TbKernel/TbConstants.h"

// Local
#include "TbTdcPixDecoder.h"

DECLARE_ALGORITHM_FACTORY(TbTdcPixDecoder)

//=============================================================================
// Standard constructor
//=============================================================================
TbTdcPixDecoder::TbTdcPixDecoder(const std::string& name,
                                 ISvcLocator* pSvcLocator)
    : TbAlgorithm(name, pSvcLocator) {
  declareProperty("HitLocation", m_hitLocation = LHCb::TbHitLocation::Default);
  declareProperty("DeviceName", m_id = "GTK0");
  declareProperty("Standalone", m_standalone = true);
  declareProperty("Monitoring", m_monitoring = true);
  // Print frequency
  declareProperty("PrintFreq", m_printFreq = 100);
}

//=============================================================================
// Destructor
//=============================================================================
TbTdcPixDecoder::~TbTdcPixDecoder() {}

//=============================================================================
// Initialisation
//=============================================================================
StatusCode TbTdcPixDecoder::initialize() {
  // Initialise the base class.
  StatusCode sc = TbAlgorithm::initialize();
  if (sc.isFailure()) return sc;

  // Make sure this device is known to the geometry service.
  m_device = geomSvc()->deviceIndex(m_id);
  const unsigned int plane = geomSvc()->plane(m_id);
  if (m_device == 999 || plane == 999) {
    return Error("Device " + m_id + " is not listed in the alignment file.");
  }
  // Append the plane index to the hit location.
  m_hitLocation += std::to_string(plane);

  // Get the list of input data.
  auto files = dataSvc()->getInputFiles();
  for (const auto& filename : files) {
    // Check the filename.
    const size_t pos1 = filename.find(".bin");
    const size_t pos2 = filename.find(".bpkt32");
    if (pos1 == std::string::npos &&
        pos2 == std::string::npos) continue;
    // Try to open the file.
    TbRawFileGTK* f = new TbRawFileGTK(filename);
    if (!f->good()) {
      if (!f->is_open()) {
        error() << "Cannot open " << filename << endmsg;
      } else {
        f->close();
      }
      warning() << "Skipping " << filename << endmsg;
      delete f;
      continue;
    }
    m_files.push_back(f);
  }
  if (m_files.empty()) return Error("No input files specified.");
  // Start with the first file in the list.
  m_currentFile = m_files.begin();
  (*m_currentFile)->initialise();

  // Get the time offset of the plane (in a not very elegant way).
  LHCb::TbHit hit;
  const uint64_t t0 = Tb::minute;
  hit.setDevice(m_device);
  hit.setTime(t0);
  hit.setPixelAddress(0);
  pixelSvc()->applyPhaseCorrection(&hit);
  const uint64_t t1 = hit.time();
  m_offset = t1 - t0;
  info() << "Applying time offset of " << m_offset << " units." << endmsg;
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode TbTdcPixDecoder::execute() {

  LHCb::TbHits* hits = getIfExists<LHCb::TbHits>(m_hitLocation);
  if (!hits) {
    // Create hit container.
    hits = new LHCb::TbHits();
    put(hits, m_hitLocation);
  }

  // Stop here if there are no more data to be read.
  if (eos()) return StatusCode::SUCCESS;

  std::vector<GTK::TDCPixTimeStamp> timeStamps;
  timeStamps.reserve(50);

  unsigned int frameCount = 0;
  bool future = false;
  if (m_standalone) {
    // Read the next frame in the file.
    (*m_currentFile)->seek(m_pos);
    if (!eos()) {
      unsigned int frameSize = 0;
      if (!readFrame(timeStamps, frameSize, frameCount)) {
        return Error("Error reading frame.");
      }
      ++m_nFrames;
      m_pos += frameSize;
      createHits(timeStamps, frameCount, hits);
      if (m_nFrames % m_printFreq == 0) printCounters(hits->size());
    }
    // Continue reading until the frame count changes.
    while (!eos()) {
      timeStamps.clear();
      unsigned int frameSize = 0;
      unsigned int frameCountNext = 0;
      (*m_currentFile)->seek(m_pos);
      if (!readFrame(timeStamps, frameSize, frameCountNext)) {
        return Error("Error reading frame.");
      }
      if (frameCountNext != frameCount) break;
      ++m_nFrames;
      m_pos += frameSize;
      createHits(timeStamps, frameCount, hits);
      if (m_nFrames % m_printFreq == 0) printCounters(hits->size());
    }  
  } else {
    // Get the event boundaries.
    uint64_t tMinEvent = 0;
    uint64_t tMaxEvent = 0;
    timingSvc()->eventDefinition(tMinEvent, tMaxEvent);
    if (UNLIKELY(msgLevel(MSG::DEBUG))) {
      debug() << format("Event window: %20u  %20u", 
                        tMinEvent, tMaxEvent) << endmsg;
    }
    unsigned int pos = m_pos;
    while (true) {
      // Read the next frame in the file.
      timeStamps.clear();
      (*m_currentFile)->seek(pos);
      if (eos()) break;
      unsigned int frameSize = 0;
      if (!readFrame(timeStamps, frameSize, frameCount)) {
        return Error("Error reading frame.");
      }
      pos += frameSize;
      // Convert the frame count to global time units.
      constexpr uint64_t frameLength = 256 * Tb::ToA;
      const uint64_t tMinFrame = frameCount * frameLength + m_offset;
      const uint64_t tMaxFrame = tMinFrame + frameLength + m_offset;
      if (UNLIKELY(msgLevel(MSG::DEBUG))) {
        debug() << format("  Frame %10u: %20u  %20u", 
                          frameCount, tMinFrame, tMaxFrame) << endmsg;
      }
      if (tMinFrame > tMaxEvent) {
        // The frame is too far in the future. Stop.
        future = true;
        break;
      } else if (tMaxFrame < tMinEvent) {
        // The frame is in the past. 
        ++m_nFrames;
        m_pos = pos;
        if (m_nFrames % m_printFreq == 0) printCounters(hits->size());
        continue;
      } 
      // The frame is overlapping with the event.
      createHits(timeStamps, frameCount, hits);
      if (tMaxFrame < tMaxEvent) {
        ++m_nFrames;
        m_pos = pos;
        if (m_nFrames % m_printFreq == 0) printCounters(hits->size());
      }
    }
  }

  // Read another frame (to catch latecomers).
  if (m_standalone || !future) { 
    (*m_currentFile)->seek(m_pos);
    if (!eos()) {
      timeStamps.clear();
      unsigned int frameSize = 0;
      unsigned int frameCountNext = 0;
      if (!readFrame(timeStamps, frameSize, frameCountNext)) {
        return Error("Error reading frame.");
      }
      createHits(timeStamps, frameCount, hits);
    }
  }

  // Sort the hits by time.
  std::sort(hits->begin(), hits->end(),
            TbFunctors::LessByTime<const LHCb::TbHit*>());
  if (UNLIKELY(msgLevel(MSG::DEBUG))) {
    info() << "Number of hits: " << hits->size() << endmsg;
    for (const LHCb::TbHit* hit : *hits) {
      info() << format("  %5d: htime = %18.3f, col = %3d, row = %3d", 
                       hit->index(), hit->htime(), hit->col(), hit->row()) 
             << endmsg; 
    }
  }
  m_nHitsRead += hits->size();
  if (eos() && m_standalone) {
    // Terminate the application.
    SmartIF<IEventProcessor> app(serviceLocator()->service("ApplicationMgr"));
    if (app) app->stopRun();
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalization
//=============================================================================
StatusCode TbTdcPixDecoder::finalize() {
  
  info() << "Processed " << m_nFrames << " frames." << endmsg;
  // Finalise the base class.
  return TbAlgorithm::finalize();
}

//=============================================================================
// Read a frame from the input file. 
//=============================================================================
bool TbTdcPixDecoder::readFrame(std::vector<GTK::TDCPixTimeStamp>& timeStamps,
                                unsigned int& frameSize, 
                                unsigned int& frameCount) {

  if (!(*m_currentFile)->good()) return false;
  // Read the header.
  std::array<uint32_t, 5> header;
  constexpr auto headerSize = header.size() * sizeof(uint32_t);
  (*m_currentFile)->read((char*)header.data(), headerSize);
  // Get the chip.
  const unsigned int qchip = header[1] & 0x3;
  const unsigned int chipId = (header[0] >> 16) & 0xff;
  // Read the payload.
  const auto payLoadLength = header[4];
  if (payLoadLength == 0) {
    error() << "Malformed packet!" << endmsg;
    return false;
  }
  frameSize = headerSize + payLoadLength * sizeof(uint32_t);
  // We read 12 bytes (two timestamps) in one iteration.
  const uint32_t nWords = (payLoadLength - 1) * sizeof(uint32_t) / 12;
  if (!(*m_currentFile)->good()) return false;

  for (unsigned int i = 0; i < nWords; ++i) {
    std::array<std::array<unsigned char, 6>, 2> buffers;
    (*m_currentFile)->readWord(buffers[0], buffers[1]);
    if (i == 0) {
      if (!decodeFrameMarker(buffers[0], frameCount)) {
        Error("Invalid frame marker (start).").ignore();
      }
      if (!decodeFrameMarker(buffers[1], frameCount)) {
        Error("Invalid frame marker (end).").ignore();
        return false;
      }
      frameCount -= 1;
      continue;
    }
    for (const auto& buf : {buffers[0], buffers[1]}) {
      GTK::TDCPixTimeStamp ts;
      ts.SetBuffer(buf.data());
      ts.SetQChip(qchip);
      ts.SetChipId(chipId);
      ts.SetFrameCount(frameCount);
      // Test for validity.
      if (!ts.IsPaddingWord() && ts.IsTimeStamp()) {
        timeStamps.emplace_back(std::move(ts));
      }
    }
  }
  return true;

}

//=============================================================================
// Decode a start/end frame marker word. 
//=============================================================================
bool TbTdcPixDecoder::decodeFrameMarker(const std::array<unsigned char, 6>& buf,
                                        unsigned int& frameCount) const {

  // Make sure these are not timestamps or padding words.
  constexpr std::array<unsigned char, 6> padding {{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}};
  if (buf == padding) {
    Error("Frame marker is a padding word.").ignore();
    return false;
  }
  if ((buf[5] & 0x80) == 0) {
    Error("Frame marker is a time stamp.").ignore();
    return false;
  } 

  // Frame count
  frameCount = *(reinterpret_cast<const unsigned int *>(buf.data())) & 0x0fffffff;
  if (UNLIKELY(msgLevel(MSG::DEBUG))) {
    // Hit count (36:28)
    const unsigned short temp0 = (0x1f & buf[4]) << 4;
    const unsigned short temp1 = (buf[3] >> 4) & 0xf;
    const unsigned short hitCount = temp1 | temp0;
    debug() << "Hit count = " << hitCount << endmsg;
    // QChip collision count (42:37)
    const unsigned short temp2 = (0x7 & buf[5]) << 3;
    const unsigned short temp3 = (buf[4] >> 5) & 0x7;
    const unsigned short collisionCount = temp2 | temp3;
    debug() << "Collision count = " << collisionCount << endmsg;
    // QChip address
    const unsigned short address = (buf[5] >> 3) & 0x3;
    debug() << "Address = " << address << endmsg;
  }
  return true;
}

//=============================================================================
// Make hit objects from the timestamps and add them to the container.
//=============================================================================
void TbTdcPixDecoder::createHits(
    const std::vector<GTK::TDCPixTimeStamp>& timeStamps,
    const unsigned int frameCount, LHCb::TbHits* hits) {

  uint64_t evtMinTime, evtMaxTime;
  timingSvc()->eventDefinition(evtMinTime, evtMaxTime);
  for (const auto& ts : timeStamps) {
    // Calculate the leading time (in ns).
    const std::pair<unsigned int, double> leadingTime = ts.GetLeadingTime();
    // Skip time stamps with the wrong frame count.
    if (leadingTime.first != frameCount) continue;
    // One frame is 256 clock cycles long. 
    constexpr uint64_t frameLength = 256 * Tb::ToA;
    const uint64_t frameTime = leadingTime.first * frameLength;
    // Convert the timestamp to global time.
    const uint64_t globalTime = frameTime + globalTimeStamp(leadingTime.second);
    if (!m_standalone) {
      // Check if the timestamp is within the event boundaries.
      if (!timingSvc()->inEvent(globalTime)) continue;
    }
    // Get the pixel address.
    const unsigned int col = ts.GetNaturalColumnIndex();
    const unsigned int row = ts.GetNaturalPixelIndex();
    const auto pixelAddress = pixelSvc()->address(col, row);
    // Skip masked pixels.
    if (unlikely(pixelSvc()->isMasked(pixelAddress, m_device))) continue;
    // Create a hit object.
    LHCb::TbHit* hit = new LHCb::TbHit();
    hit->setDevice(m_device);
    auto buffer = ts.GetBuffer();
    const uint64_t packet = ((uint64_t)buffer[0] << 40) | 
                            ((uint64_t)buffer[1] << 32) |
                            ((uint64_t)buffer[2] << 24) |
                            ((uint64_t)buffer[3] << 16) |
                            ((uint64_t)buffer[4] <<  8) |
                            ((uint64_t)buffer[5] <<  0);
    hit->setData(packet);
    hit->setCol(col);
    hit->setRow(row);
    hit->setScol(col);
    hit->setPixelAddress(pixelAddress);
    // TODO!
    hit->setToT(1);
    // hit->setCharge(pixelSvc()->charge(tot, pixelAddress, device));
    hit->setCharge(1.);
    hit->setTime(globalTime);
    // pixelSvc()->applyPhaseCorrection(hit);
    hit->setHtime(timingSvc()->globalToLocal(globalTime));
    // Add the hit to the container.
    hits->insert(hit);
  }

}

//=============================================================================
// Check if all input files have been read. Move to the next file if needed. 
//=============================================================================
bool TbTdcPixDecoder::eos() {

  if (unlikely(m_currentFile == m_files.end())) return true;
  if (unlikely((*m_currentFile)->eof())) {
    // Move to the next file in the sequence.
    (*m_currentFile)->close();
    ++m_currentFile;
    if (m_currentFile != m_files.end()) {
      (*m_currentFile)->initialise();
      // Reset the position.
      m_pos = 0;
    }
  }
  return unlikely(m_currentFile == m_files.end());
}

