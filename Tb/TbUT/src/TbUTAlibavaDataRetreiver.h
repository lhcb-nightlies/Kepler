/*
 * TbUTAlibava.h
 *
 *  Created on: Oct 1, 2014
 *      Author: ADendek
 */

#pragma once

#include "alibava/TbAsciiRoot.h"
#include "TbUTIDataRetreiver.h"

namespace TbUT {
class AlibavaDataRetreiver : public IDataRetreiver {
 public:
  AlibavaDataRetreiver();
  void open(std::string& p_filename) override;
  bool valid() override;
  int read_event(std::string& error_code) override;
  void process_event() override;
  double time() override;
  double temp() override;
  unsigned short data(int i) override;

 private:
  TbAsciiRoot m_assciRoot;
};
}
