#include <iostream>

// ROOT
#include "TFile.h"

/* @class TbBufferedFile TbBufferedFile.h
 *
 * Generic buffered interface to ROOT IO for use of IO plugins
 *
 */

// BUFFERSIZE is the number of objects that can be buffered
template <unsigned int BUFFERSIZE, class TYPE>
class TbBufferedFile {

 public:
  /// Constructor
  TbBufferedFile(const std::string& filename, bool init = true)
      : m_file(TFile::Open((filename + "?filetype=raw").c_str())),
        m_filename(filename) {
    if (is_open()) {
      m_bytes = m_file->GetSize();
      if (init) initialise();
    } else {
      std::cout << "File opening failed - waiting 5s then reconnecting\n";
      sleep(5);
      m_file = TFile::Open((filename + "?filetype=raw").c_str());
      if (is_open()) {
        m_bytes = m_file->GetSize();
        if (init) initialise();
      } else {
        std::cerr << "Cannot open file: " << filename << " critical error!\n";
      }
    }
  }
  /// Destructor
  ~TbBufferedFile() { close(); }

  /// Sets the offset in the file and loads the corresponding buffer (IN BYTES)
  void setOffset(const uint64_t pos) {
    if (pos < m_bytesRead && pos > m_bytesRead - m_bufferSize) {
      m_bufferPos = (pos - (m_bytesRead - m_bufferSize)) / sizeof(TYPE);
      return;
    }
    if (pos > m_bytes) {
      std::cout << "Attempted to read outside of file, offsetting cancelled\n";
      return;
    }
    m_file->SetOffset(pos, TFile::ERelativeTo::kBeg);
    m_bufferSize = std::min(m_bytes - m_bytesRead, BUFFERSIZE * sizeof(TYPE));
    m_file->ReadBuffer((char*)m_buffer, m_bufferSize);
    m_bufferPos = 0;
    m_bytesRead = pos + m_bufferSize;
  }

  TYPE getNext() {

    if (__builtin_expect(m_bufferPos * sizeof(TYPE) == m_bufferSize, 0)) {
      // Unlikely case. Read new buffer.
      m_bufferSize = std::min(m_bytes - m_bytesRead, BUFFERSIZE * sizeof(TYPE));
      if (m_file->ReadBuffer((char*)m_buffer, m_bufferSize)) {
        /// this is to give additional protection in case of network timeouts
        std::cout << "Reading the buffer of " << m_filename
                  << " failed. Waiting for 5s" << std::endl;
        sleep(5);
        m_file->SetOffset(-m_bufferSize, TFile::ERelativeTo::kCur);
        if (m_file->ReadBuffer((char*)m_buffer, m_bufferSize)) {
          std::cout << "Reading the buffer failed again, closing file\n";
          m_bytesRead = m_bytes;
          m_bufferPos = m_bufferSize / sizeof(TYPE);
          return TYPE();
        }
      }
      m_bytesRead += m_bufferSize;
      m_bufferPos = 0;
    }
    return m_buffer[m_bufferPos++];
  }
  TYPE getPrevious() {
    if (m_bufferPos > 0) {
      m_bufferPos -= 1;
      return m_buffer[m_bufferPos];
    }
    return TYPE();
  }
  bool eof() const {
    return (m_bytesRead == m_bytes) &&
           (m_bufferPos * sizeof(TYPE) == m_bufferSize);
  }
  void close() {
    if (m_init) {
      delete[] m_buffer;
      m_init = false;
    }
    if (is_open()) m_file->Close();
  }

  void reset() {
    if (m_init) {
      delete[] m_buffer;
      m_init = false;
    }
    m_bytesRead = 0;
    m_bufferSize = BUFFERSIZE * sizeof(TYPE);
    m_bufferPos = BUFFERSIZE;
  }
  bool is_open() const { return m_file != 0 && m_file->IsOpen(); }

  void initialise() {
    if (!m_init) {
      m_buffer = new TYPE[BUFFERSIZE];
      m_init = true;
    }
  }

 protected:
  TFile* m_file = nullptr;
  uint64_t m_bytesRead = 0;
  std::string m_filename;

 private:

  uint64_t m_bufferSize = BUFFERSIZE * sizeof(TYPE);
  uint64_t m_bufferPos = BUFFERSIZE;
  uint64_t m_bytes = 0;
  TYPE* m_buffer = nullptr;
  bool m_init = false;
};
